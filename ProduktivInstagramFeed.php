<?php 
    
    class ProduktivInstagramFeed {
        public $options;

        function __construct($options) {
            $this->options = $options;
        }
        
        function fetchInstagramFeed() {
            $instaApi = "https://api.instagram.com/v1/users/self/media/recent/?access_token={$this->options['auth_token']}&count={$this->options['limit']}";
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $instaApi);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20);
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;            
        }

        public function getInstagramFeed() { 
            return $this->fetchInstagramFeed();
        }
    }   
?>